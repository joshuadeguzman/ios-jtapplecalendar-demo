//
//  Event.swift
//  ios-jtapplecalendar-demo
//
//  Created by Joshua de Guzman on 07/06/2018.
//  Copyright © 2018 xrojan. All rights reserved.
//

import Foundation

struct Event{
    var name: String!
    var date: Date!
    var type: String!
}
