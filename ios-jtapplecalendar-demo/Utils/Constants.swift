//
//  Constants.swift
//  ios-jtapplecalendar-demo
//
//  Created by Joshua de Guzman on 07/06/2018.
//  Copyright © 2018 xrojan. All rights reserved.
//

import Foundation

class Keys{
    static let BIRTHDAY = "BIRTHDAY"
    static let HOLIDAY = "HOLIDAY"
    static let SPECIAL_HOLIDAY = "SPECIAL_HOLIDAY"
}
